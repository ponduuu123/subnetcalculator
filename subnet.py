import re

class SubnetCalculator:
    def __init__(self, ip_address, subnet_mask):
        self.ip_address = ip_address
        self.subnet_mask = subnet_mask
        self.subnet_bits = 0
        self.max_subnets = 0
        self.hosts_per_subnet = 0
        self.network_address = 0
        self.broadcast_address = 0
        self.first_host_address = 0
        self.last_host_address = 0
        self.subnet_id = ""
        self.hex_ip_address = ""
        self.wildcard_mask = ""
        self.subnet_bitmap = ""
        self.mask_bits = ""
        self.first_octet_range = ""
        self.prefix = ""
        self.calculate_subnet_details()

    def is_valid_ip_address(self, ip_address):
        ipv4_pattern = r'^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}$'
        return bool(re.match(ipv4_pattern, ip_address))

    def is_valid_subnet_mask(self, subnet_mask):
        subnet_pattern = r'^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}$'
        return bool(re.match(subnet_pattern, subnet_mask))

    def calculate_subnet_details(self):
        if self.is_valid_ip_address(self.ip_address) and self.is_valid_subnet_mask(self.subnet_mask):
            ip_parts = list(map(int, self.ip_address.split('.')))
            mask_parts = list(map(int, self.subnet_mask.split('.')))
            ip = 0
            mask = 0

            for i in range(4):
                ip |= ip_parts[i] << (24 - 8 * i)
                mask |= mask_parts[i] << (24 - 8 * i)

            self.subnet_bits = bin(mask).count('1')
            self.max_subnets = 2 ** (32 - self.subnet_bits)
            self.hosts_per_subnet = (2 ** (32 - self.subnet_bits)) - 2
            step = 2 ** (32 - self.subnet_bits)

            self.network_address = ip & mask
            self.broadcast_address = self.network_address + step - 1

            self.first_host_address = self.network_address + 1
            self.last_host_address = self.broadcast_address - 1

            self.subnet_id = self.long_to_ip(self.network_address)
            self.hex_ip_address = self.long_to_hex(ip)
            self.wildcard_mask = self.calculate_wildcard_mask(mask)
            self.first_octet_range = self.calculate_first_octet_range(ip, mask)
            self.subnet_bitmap = self.calculate_subnet_bitmap(mask)
            self.mask_bits = self.calculate_mask_bits(mask)

        else:
            print("Invalid IP address or subnet mask format.")
            exit(1)

    def long_to_ip(self, ip):
        return f"{(ip >> 24) & 0xFF}.{(ip >> 16) & 0xFF}.{(ip >> 8) & 0xFF}.{ip & 0xFF}"

    def long_to_hex(self, ip):
        return f"{(ip >> 24) & 0xFF:02X}:{(ip >> 16) & 0xFF:02X}:{(ip >> 8) & 0xFF:02X}:{ip & 0xFF:02X}"

    def calculate_wildcard_mask(self, mask):
        wildcard = 0xFFFFFFFF - mask
        return self.long_to_ip(wildcard)

    def calculate_subnet_bitmap(self, mask):
        subnet_bitmap = self.prefix
        bit = 1 << 30
        if self.prefix == "0":
            for i in range(1, 32):
                if i % 8 == 0 and i != 0:
                    subnet_bitmap += "."
                if mask & bit != 0:
                    subnet_bitmap += "n"
                else:
                    subnet_bitmap += "h"
                bit >>= 1
        elif self.prefix == "10":
            for i in range(1, 32):
                if i % 8 == 0 and i != 0:
                    subnet_bitmap += "."
                if mask & bit != 0:
                    subnet_bitmap += "n"
                    if i == 4:
                        subnet_bitmap = subnet_bitmap[:4] + subnet_bitmap[5:]
                else:
                    subnet_bitmap += "h"
                bit >>= 1
        elif self.prefix == "110":
            for i in range(1, 32):
                if i % 8 == 0 and i != 0:
                    subnet_bitmap += "."
                if mask & bit != 0:
                    subnet_bitmap += "n"
                    if i == 4:
                        subnet_bitmap = subnet_bitmap[:4] + subnet_bitmap[5:]
                    if i == 5:
                        subnet_bitmap = subnet_bitmap[:5] + subnet_bitmap[6:]
                else:
                    subnet_bitmap += "h"
                bit >>= 1
        return subnet_bitmap

    def calculate_mask_bits(self, mask):
        mask_bit_count = 0
        bit = 1 << 31
        while mask_bit_count < 32 and (mask & bit) != 0:
            mask_bit_count += 1
            bit >>= 1
        return str(mask_bit_count)

    def calculate_first_octet_range(self, ip, mask):
        first_octet = (ip & mask) >> 24
        if 1 <= first_octet <= 126:
            self.prefix = "0"
            return "1 - 126"
        elif 128 <= first_octet <= 191:
            self.prefix = "10"
            return "128 - 191"
        elif 192 <= first_octet <= 223:
            self.prefix = "110"
            return "192 - 223"
        elif 224 <= first_octet <= 239:
            return "Multicast"
        elif 240 <= first_octet <= 255:
            return "Reserved"
        else:
            return "Not in the defined IP address classes"

    def display_subnet_details(self):
        print("IP Address:", self.ip_address)
        print("Hex IP Address:", self.hex_ip_address)
        print("Subnet Mask:", self.subnet_mask)
        print("Subnet Bits:", self.subnet_bits)
        print("Maximum Subnets:", self.max_subnets)
        print("Hosts per Subnet:", self.hosts_per_subnet)
        print("Subnet ID:", self.subnet_id)
        print("Host Address Range:", self.long_to_ip(self.first_host_address), "-", self.long_to_ip(self.last_host_address))
        print("Broadcast Address:", self.long_to_ip(self.broadcast_address))
        print("Wildcard Mask:", self.wildcard_mask)
        print("Subnet Bitmap:", self.subnet_bitmap)
        print("Mask Bits:", self.mask_bits)
        print("First Octet Range:", self.first_octet_range)

if __name__ == "__main__":
    ip_address = input("Enter IP address: ")
    subnet_mask = input("Enter subnet mask: ")

    network = SubnetCalculator(ip_address, subnet_mask)
    network.display_subnet_details()
